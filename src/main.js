var React = require('react');
var ReactDOM = require('react-dom');
var Timer = require('./components/timer');
var pomodoro = [1500, 300, 1500, 300, 1500, 300, 1500, 900];
var labels = ['Work!', 'Short Break', 'Work!', 'Short Break', 'Work!', 'Short Break', 'Work!', 'Long Break'];

ReactDOM.render(
  <Timer pomodoro={pomodoro} labels={labels} />,
  document.getElementById('content')
);
