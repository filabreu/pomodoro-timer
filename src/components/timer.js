var React = require('react');
var TimerCounter = require('./timer_counter');
var TimerControl = require('./timer_control');

var Timer = React.createClass({
  getInitialState: function() {
    return { playing: false, seconds: 0, step: 0, label: 'Star a new Pomodoro!' }
  },
  startTimer: function() {
    this.setState({ playing: true, seconds: this.props.pomodoro[this.state.step], label: this.props.labels[this.state.step] });
  },
  stopTimer: function() {
    this.setState({ playing: false, seconds: 0, step: 0, label: 'Star a new Pomodoro!' });
  },
  nextStep: function() {
    var step = this.state.step + 1;
    if(step < this.props.pomodoro.length) {
      this.setState({ playing: true, seconds: this.props.pomodoro[step], step: step, label: this.props.labels[step] });
    } else {
      this.stopTimer();
    }
  },
  render: function() {
    return(
      <div className="timer">
        <h3>{this.state.label}</h3>
        <TimerCounter seconds={this.state.seconds} onTimerFinish={this.nextStep} />
        <hr />
        <TimerControl playing={this.state.playing} onStart={this.startTimer} onStop={this.stopTimer} />
      </div>
    );
  }
});

module.exports = Timer;
