var React = require('react');

var TimerControl = React.createClass({
  start: function(e) {
    this.props.onStart();
  },
  stop: function(e) {
    this.props.onStop();
  },
  render: function() {
    return(
      <div className="timerControl">
        { this.props.playing
          ? <button type="button" id="timerStop" onClick={this.stop}>Stop Pomodoro</button>
          : <button type="button" id="timerStart" onClick={this.start}>Start Pomodoro</button>
        }
      </div>
    );
  }
});

module.exports = TimerControl;
