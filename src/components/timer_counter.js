var React = require('react');

var TimerCounter = React.createClass({
  buildCounter: function(time) {
    var minutes = Math.floor(time / 60);
    var seconds = time - (minutes * 60);
    if(minutes < 10) {
      minutes = "0" + minutes;
    }
    if(seconds < 10) {
      seconds = "0" + seconds;
    }
    return(minutes + ":" + seconds);
  },
  setCounter: function(seconds) {
    var timeout;
    clearTimeout(this.state.timeout);
    if(seconds) {
      timeout = setTimeout(this.count, 1000);
    }
    this.setState({ counter: this.buildCounter(seconds), seconds: (seconds - 1), timeout: timeout });
  },
  count: function() {
    if(this.state.seconds) {
      this.setCounter(this.state.seconds);
    } else {
      this.props.onTimerFinish();
    }
  },
  componentWillReceiveProps: function(nextProps) {
    this.setCounter(nextProps.seconds);
  },
  getInitialState: function() {
    return { counter: "00:00" };
  },
  render: function() {
    return(
      <div className="timerCounter">{this.state.counter}</div>
    );
  }
})

module.exports = TimerCounter;
