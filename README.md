# Pomodoro Timer - Teste para Nossas Cidades

## Dependências

Para essa aplicação é necessário instalar o NPM (Node.js Packager Manager).
[https://docs.npmjs.com/cli/install]

Também será necessário o git para realizar o clone do projeto.
[https://git-scm.com/book/en/v2/Getting-Started-Installing-Git]

## Setup

Primeiro faça o clone do projeto

```
git clone https://filabreu@bitbucket.org/filabreu/pomodoro-timer.git
```

Instale as dependências utilizando o NPM

```
cd pomodoro-timer && npm install
```

Ainda na raiz do projeto, faça o build do projeto utilizando o webpack

```
webpack
```

Abra o arquivo index.html no seu navegador. Em MacOSX ou Linux é possíve fazer isso do terminal.

```
open index.html
```

Ao abrir o seu navegador, clique no botão 'Start Pomodoro!'.

Enjoy!
